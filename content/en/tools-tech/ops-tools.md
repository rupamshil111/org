---
title: Operations Tools
description: An overview of the Operations Tools we use at Grey Software
category: Tools and Tech
position: 16
---

## Plausible Analytics

![Plausible Preview](/tech-stack/plausible-preview.png)

<cta-button  link="http://org.grey.software/analytics" text="Our Analytics" > </cta-button>

## HackMD

![HackMD Preview](/tech-stack/hackmd-preview.png)

HackMD allows you to work on Markdown files and collaborate with your teammates in real time. At Grey Software, we use HackMD as our Google Docs or Microsoft Word alternative for collaborating on team documents.

<cta-button link="https://hackmd.io/join" text="Sign Up" > </cta-button>

## N8N

![N8N Preview](/tech-stack/n8n-preview.png)

Workflow autmation allows our organizational processes to function with fewer errors and less time by having computers execute rule-based logic that automates manual work.

At Grey Software, we love using N8N to automate our workflows because of its fun user-interface and commitment to open-source transparency.

<cta-button link="https://n8n.io" text="Learn More" > </cta-button>
